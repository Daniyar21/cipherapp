import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {decodeData, encodeData, onChangeDecode, onChangeEncode, onPasswordChange} from "./store/actions/cipherActions";

const App = () => {
    const dispatch = useDispatch();

    const decode = useSelector(state => state.decode);
    const encode = useSelector(state => state.encode);
    const password = useSelector(state => state.password)


    const toEncode = {encode:encode, password:password}
    const toDecode = {decode:decode, password:password}

    const onEncode=()=>{
        dispatch(encodeData(toEncode));
    }
    const onDecode=()=>{
        dispatch(decodeData(toDecode));
    }


    return (
      <div className="App">
            <p>Encode message</p>
          <textarea
              name="encode"
              id="encode" cols="30" rows="10"
              onChange={e=>dispatch(onChangeEncode(e.target.value))}
              value={encode}
          ></textarea>
          <p>Password</p>
          <input
              type="text" name="password"
              onChange={e=>dispatch(onPasswordChange(e.target.value))}
              value={password}

          />
          <button onClick={onEncode}>Encode</button>
          <button onClick={onDecode}>Decode</button>
          <p>Decode message</p>
          <textarea
              name="decode"
              id="decode" cols="30" rows="10"
              onChange={e=>dispatch(onChangeDecode(e.target.value))}
              value={decode}
          ></textarea>
      </div>
  );
};

export default App;
