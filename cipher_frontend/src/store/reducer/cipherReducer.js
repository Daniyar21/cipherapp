import {
    DECODE_SUCCESS,
    ENCODE_SUCCESS,
    ON_CHANGE_DECODE,
    ON_CHANGE_ENCODE,
    ON_PASSWORD_CHANGE
} from "../actions/cipherActions";

const initialState = {
    encode: '',
    decode: '',
    password: '',
}

const reducer = (state= initialState, action)=>{
    switch (action.type){

        case ENCODE_SUCCESS:
            return {...state, encode: action.payload};
        case DECODE_SUCCESS:
            return {...state, decode: action.payload};
        case ON_CHANGE_DECODE:
            return {...state, decode: action.payload};
        case ON_CHANGE_ENCODE:
            return {...state, encode: action.payload};
        case ON_PASSWORD_CHANGE:
            return {...state, password: action.payload};
        default:
            return state;
    }
}

export default reducer;