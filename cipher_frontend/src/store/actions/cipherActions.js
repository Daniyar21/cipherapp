import axios from "axios";

export const ENCODE_SUCCESS = 'ENCODE_SUCCESS';
export const DECODE_SUCCESS = 'DECODE_SUCCESS';
export const ON_CHANGE_ENCODE = 'ON_CHANGE_ENCODE';
export const ON_CHANGE_DECODE = 'ON_CHANGE_DECODE';
export const ON_PASSWORD_CHANGE = 'ON_PASSWORD_CHANGE';

export const encodeSuccess = data => ({type: ENCODE_SUCCESS, payload: data});
export const decodeSuccess = data => ({type: DECODE_SUCCESS, payload: data});
export const onChangeEncode = data => ({type: ON_CHANGE_ENCODE, payload: data});
export const onChangeDecode = data => ({type: ON_CHANGE_DECODE, payload: data});
export const onPasswordChange = data => ({type: ON_PASSWORD_CHANGE, payload: data})

export const encodeData = data => {
    return async dispatch => {
        try {
            const response = await axios.post('http://127.0.0.1:8008/encode', data);
            dispatch(encodeSuccess(response.data))
        } catch (error) {
            console.log('error', error);
        }
    }
}

export const decodeData = data => {
    return async dispatch => {
        try {
            const response = await axios.post('http://127.0.0.1:8008/decode', data);
            dispatch(decodeSuccess(response.data))
        } catch (error) {
            console.log('error', error);
        }
    }
}